module.exports = class Route {
    constructor() {
        let app = express();
        app.use(express.static('views'));
        app.set('views', _dirname + '/views');
        app.engine('html', require('ejs').renderFile);
        app.use(session({secret: 'ssshhhhh'}));

        let BaseController = require(_dir_controllers + "baseController");
        let baseController = new BaseController();
        global.baseController = baseController;
        app.get(new RegExp('(?:' + _expansion + ')'), (req, res) => {
            let dir = fs.readFileSync(_dirname + req.url);
            res.setHeader('Content-Type', 'text/js');
            res.end(dir);
        });

        let modules = fs.readdirSync(_dir_modules);
        let def = [];

        for (var prop in modules) {
            app.get(new RegExp('(^\/['+modules+']{0,}\/[A-Z a-z]{0,})'), (req,res) => {
                
            });
        }

        app.get("/", (req,res) => { baseController.index(req,res) });

        return app;
    }
}