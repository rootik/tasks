class Socket {
    constructor() {
        var socket;
        var room = {};
        this.socket = require('socket.io-client')('http://localhost:3500');
        socket = this.socket;

        socket.on("start", (data)=>{
            var check = data.game;

            for (var prop in check) {
                if(check.hasOwnProperty(prop)){
                    if (localStorage.getItem("session_users") == check[prop]) {
                        room[data._id] = {
                            room: data.game,
                            id:data._id
                        }

                        localStorage.setItem("id_room", data._id);
                        localStorage.setItem("room", JSON.stringify(room));
                        localStorage.setItem("template", "roomGames");
                        localStorage.setItem("start_game", true);
                        render.render();
                    }
                }
            }
        });

        return socket;
    }
}

const socket = new Socket();
export default socket;