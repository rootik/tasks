class Render {
    render() {
        var tpl = "";
        localStorage.getItem('template') ? tpl = localStorage.getItem("template") : tpl = "auth";
        document.querySelector("div").setAttribute("id", tpl);
        switch (tpl) {
          case "app": this.renderApp(); break;
          case "auth": this.renderAuth(); break;
          case "roomGames": this.renderRoomGames(); break;
        }

    }

    renderApp() {
      var App = require('../../ui/components/app');
      RenderDOM.render(<App />, document.getElementById('app'));
    }

    renderAuth() {
      var App = require('../../ui/components/auth');
      RenderDOM.render(<App />, document.getElementById('auth'));
    }

    renderRoomGames() {
      var App = require('../../ui/components/games');
      RenderDOM.render(<App />, document.getElementById('roomGames'));
    }
}

const render = new Render();
export default render;