import Dispatcher from 'flux-dispatcher';
import emit from 'event-emitter';
import EventEmitter from 'events';

class Store extends EventEmitter{
    constructor(dispather) {
        super();
        this.dispatcher = Dispatcher;
    }
}

module.exports = Store;