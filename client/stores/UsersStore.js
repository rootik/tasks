class StoreUsers extends Store{
    constructor() {
        super({
            models: ['Users'],
            stores: ['Users']  
        });
        this._items = ['asdassdasad'];

        this._dataUsers = {
            email:"none",
            photo:"none"
        };

        this.dispatcher.register(this.handleActions.bind(this));
        global.dispatcher = this.dispatcher;
    }

    _set(item) {
        this._items.push(item);
        this.emit("change");
    }

    _setUsers(params) {
        params = params[0];
        this._dataUsers.email = params.email;
        this._dataUsers.photo = params.photo;
        this.emit("change");
    }

    _getUsers() {
        return this._dataUsers;
    }

    _get() {
        return this._items;
    }

    handleActions(action) {
        switch(action.type) {
            case "GET_LINK": { this._set(action.item) }
            case "GET_DATA": { this._setUsers(action.item) }
        }
    }
}

const usersStore = new StoreUsers;
module.exports = usersStore;