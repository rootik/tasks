class Games extends Model{
    constructor() {
        super();
        global.result = {};

        if(!localStorage.getItem("session_users")) localStorage.removeItem("start_game");
    }

    setButton() {
        let button = document.getElementById("clickGame");
        
        button.innerHTML = "Ожидайте";
        button.setAttribute("disabled", "true");
    }

    startGame() {
        let button = document.getElementById("clickGame");
        button.innerHTML = "Ожидайте";
        button.setAttribute("disabled", "true");
        localStorage.setItem("check", true);
        Socket.emit("start game", {
            game:'default',
            id:localStorage.getItem("session_users")
        });
    }

    circle() {
        var context = this.context;
        context.beginPath();
        context.arc(75,75,50,0,Math.PI*2,true); // Внешняя окружность
        context.moveTo(0,0);
        context.stroke();
    }

    update() {
        var mouseX = this.mouseX,
            mouseY = this.mouseY;
        
        function update(){
            context.beginPath();
            context.arc(mouseX, mouseY, 1, 0, 2 * Math.PI, true);
            context.fillStyle = "#FF6A6A";
            context.fill();
        }

        requestAnimationFrame(update);
    }

    getPosition(el) {
        var xPosition = 0;
        var yPosition = 0;

        while (el) {
            xPosition += (el.offsetLeft - el.scrollLeft + el.clientLeft);
            yPosition += (el.offsetTop - el.scrollTop + el.clientTop);
            el = el.offsetParent;
        }
        return {
            x: xPosition,
            y: yPosition
        };
    }

    drawCircle(data) {
        context.beginPath();
        context.arc(data.X, data.Y, 2, 0, 2 * Math.PI, true);
        context.fillStyle = "#FF6A6A";
        context.fill();
    }

    getCanvas() {
        var canvas = document.querySelector("#myCanvas"),
            left = document.getElementById("mainContainer");
            global.l = 1;
        if(canvas) {
            var canvasPos = this.getPosition(canvas),
                context = canvas.getContext("2d");
            global.context = context;
            this.context = context;
            this.mouseX = 0;
            this.mouseY = 0;

            context.fillStyle = "#FF6A6A";
            context.fill();

            this.circle();
            this.update();

            canvas.addEventListener("mousedown", function(){
                canvas.addEventListener("mousemove", setMousePosition);
            });

            canvas.addEventListener("mouseup", function(){
                canvas.removeEventListener("mousemove", setMousePosition);
            });

            Socket.on("draw circle",(data)=>{
                this.drawCircle(data);
            });

            function setMousePosition(e) {
                var z = 1,mouseX,mouseY;
                this.mouseX = e.clientX - (canvas.offsetLeft + left.offsetLeft)-50;
                this.mouseY = e.clientY-canvas.offsetTop - 50;
                l = l + 1;

                z = mouseY - mouseX;

                global.radius = l/Math.PI;

                Socket.emit("draw circle", {
                    X:this.mouseX,
                    Y:this.mouseY
                });


                context.beginPath();
                context.arc(this.mouseX, this.mouseY, 2, 0, 2 * Math.PI, true);
                context.fillStyle = "#FF6A6A";
                context.fill();
            }
        }
    }

    clearCanvas() {
        context.clearRect(0, 0, 500, 500);
        this.circle();
    }

    selectPlayers(player) {
    }

    CalculationResult() {
        var radius = global.radius;
        if(radius) {
            var perfectResultat = 100, result, finalResult;
            result = Number(radius);
            
            result > perfectResultat ? finalResult = (perfectResultat*100)/result : finalResult = (result*100)/perfectResultat;
            l = 0;

            return finalResult;
        }
    }

    
    timePlayers(params) {
        if(!localStorage.getItem("id_room")) return false;

        var block = params.block,
            seconds = Number(block.innerHTML) - 1,
            l = this.numbers,
            room = JSON.parse(localStorage.getItem("room"))[localStorage.getItem("id_room")],
            players = document.getElementsByClassName("users_block"),
            namePlayers = players[l].getElementsByClassName("name")[0].innerHTML,
            blockNamePlayers = document.getElementById("drawPlayers").innerHTML = namePlayers,
            canvas = document.getElementById("myCanvas");

        if(seconds<=10 && seconds >= 5) params.clock.classList.add("clock_middle");
        if(seconds<=5) params.clock.classList.add("clock_danger");
        
        for (var prop in players) {
            if(players.hasOwnProperty(prop)) players[prop].classList.remove("selectPlayer");
        }

        var room = room.room[l];

        canvas.classList.remove("block_canvas");
        if(room != localStorage.getItem("session_users"))  canvas.classList.add("block_canvas");

        if(seconds == 0){
            var result = this.CalculationResult();
            if (room == localStorage.getItem("session_users")) this.saveResult(result);

            if(this.numbers == 2) {
                this.endGame(room.id);
                return false;
            }else{
                this.clearCanvas();

                Socket.emit("start new game", {
                    id: localStorage.getItem("id_room"),
                    number: this.numbers,
                    users: localStorage['session_users'],
                    result: result
                });
            }
        }

        players[l].classList.add("selectPlayer");
        
        block.innerHTML = seconds;
    }

    saveResult(result) {
        Socket.emit("save results", {
            users: localStorage.getItem("session_users"),
            result: result
        });
    }

    endGame(id) {
        this.numbers = 0;
        Socket.emit("end game", {
            _id: id
        });

        this.showWindowResults();

        for (var prop in localStorage) {
            if(prop!="get_data") {
                if(prop!="session_users") {
                    localStorage.removeItem(prop);
                }
            }
        }
    }

    showWindowResults() {
        Socket.on("end game", (data)=>{
            dispatcher.dispatch({type:"SET_PLAYERS", item:{better: data.better, results: data.overallResult}});
        });
        document.getElementById("mainContainer").classList.add("content_blur");
        document.getElementById("hover").classList.add("show_hover");
        document.getElementById("closeGame").addEventListener("click", ()=>{
            localStorage.setItem("template", "app");
            render.render();
        });
    }

    queuePlayers() {
        var vars = {},
            data = {},
            clock = document.getElementById("clock"),
            seconds = document.getElementById("seconds");

        if(clock) {
            vars.seconds = seconds.innerHTML;
            data.seconds = vars.seconds;
            data.block = seconds;
            data.clock = clock;
            localStorage.setItem("roomGames", JSON.stringify(vars));
            this.numbers = 0;

            Socket.on("next player", (data)=>{
                this.numbers = data;
                seconds.innerHTML = 15;
            });

            var interval = setInterval(()=>{
                var result = this.timePlayers(data);

                if(result === false) clearInterval(interval); 
            },1000);
        }
    }

    startGameRoom() {
        this.getCanvas();
        this.queuePlayers();
    }
}

const games = new Games;
export default games;