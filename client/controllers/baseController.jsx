import React from 'react';
import RenderDOM from 'react-dom';
import Router from 'react-router';
import Socket from '../classes/socket';
import Dispather from 'flux-dispatcher';
import EventEmitter from 'event-emitter';
import injectTapEventPlugin from 'react-tap-event-plugin';
injectTapEventPlugin();

(function() {
  var baseUI;

  module.exports = baseUI = (function() {
    function baseUI() {
      require("../views/stylesheet/_bootstrap.scss");
      require("../views/stylesheet/style.scss");
      require("../views/stylesheet/font-awesome/scss/font-awesome.scss");

      global.React = React;
      global.RenderDOM = RenderDOM;
      global.Model = require("../classes/model");
      global.Store = require("../classes/store");
      global.Socket = Socket;
      global.render = require("../classes/render").default;
      var xhr = new XMLHttpRequest();
      xhr.open("GET", "/users/getSessions");
      xhr.send();

      xhr.onreadystatechange = ()=>{
          if (xhr.readyState!=4) return;
          var response = JSON.parse(xhr.responseText);
          
          if(response.res===true){
            localStorage.setItem('session_users', response.session);
            localStorage.getItem("room") ? localStorage.setItem("template", "roomGames") : localStorage.setItem("template", "app");
          }else{
            for (var prop in localStorage) {
                localStorage.removeItem(prop);
            }

          }
          render.render();
      }
    }

    return baseUI;

  })();

}).call(this);